const express = require('express');

const app = express();
const port = 8000;

const Vigenere = require('caesar-salad').Vigenere;

app.get('/encode/:someword', (req, res) => {

 const resultC = Vigenere.Cipher('password').crypt(req.params.someword);

  res.send(resultC);
});

app.get('/decode/:someword', (req, res) => {

  const resultD = Vigenere.Decipher('password').crypt(req.params.someword);

  res.send(resultD);
});




app.listen(port, () => {
  console.log('Server started on ' + port);
});